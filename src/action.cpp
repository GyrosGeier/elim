#include "action.h"

#include <cassert>

void action_impl::start()
{
	switch(get_action_state())
	{
	case CREATED:
		set_action_state(WAITING);
		break;
	case READY:
		do_start();
		break;
	case WAITING:
		assert(! "start() called on waiting action");
		break;
	case STARTED:
		assert(! "start() called on started action");
		break;
	case FINISHED:
		assert(! "start() called on finished action");
		break;
	}
}

void action_impl::add_dependency(event *e)
{
	auto s = get_action_state();
	switch(s)
	{
	case CREATED:
	case READY:
	case WAITING:
		/* okay, not actually running yet */
		break;
	case STARTED:
		assert(! "add_dependency() called on started action");
		break;
	case FINISHED:
		assert(! "add_dependency() called on finished action");
		break;
	}

	if(e->get_event_state() == e->SIGNALED)
		return;

	auto &ne = waiting.emplace_back(e);

	if(e->add_observer(this) == e->SIGNALED)
		ne = nullptr;
	else if(s == READY)
		set_action_state(CREATED);
}

void action_impl::final_construct()
{
	register_with_dependencies();
}

void action_impl::register_with_dependencies()
{
	auto num = waiting.size();

	for(auto &i : waiting)
		if(i->add_observer(this) == event::SIGNALED)
		{
			i = nullptr;
			--num;
		}

	if(num == 0)
		set_action_state(READY);
}

void action_impl::set_action_state(action_state new_state)
{
	action_state old_state = current_state;

	assert(new_state > old_state
		or (old_state == READY
			and new_state == CREATED));

	current_state = new_state;
	switch(new_state)
	{
		case CREATED:
		case READY:
		case WAITING:
		case STARTED:
			this->set_state(UNSIGNALED);
			break;
		case FINISHED:
			this->set_state(SIGNALED);
	}

	for(auto o : observers)
		o->on_state_change(this, old_state, new_state);
}

void action_impl::on_state_change(
		event *a,
		event::event_state,
		event::event_state new_state)
{
	if(new_state != event::SIGNALED)
		return;

	bool empty = true;
	bool removed = false;

	for(auto &i : waiting)
	{
		if(removed and not empty)
			break;
		if(i == a)
		{
			i = 0;
			removed = true;
		}
		else if(i != nullptr)
			empty = false;
	}

	if(empty and get_action_state() == WAITING)
		do_start();
}
