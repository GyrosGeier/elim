#pragma once

#include "event.h"
#include "ptr.h"

#include <ranges>
#include <list>
#include <vector>
#include <string>

class action_observer;

class action :
	public virtual event
{
public:
	virtual std::string const &name() const = 0;

	virtual void start() = 0;

	enum action_state
	{
		CREATED,
		READY,
		WAITING,
		STARTED,
		FINISHED
	};

	virtual action_state get_action_state() const = 0;

	virtual action_state add_observer(action_observer *o) = 0;
	virtual void remove_observer(action_observer *o) = 0;

	virtual void add_dependency(event *) = 0;
};

class action_impl :
	public virtual action,
	private event_impl,
	private event_observer
{
public:
	void start() final;

	action_state get_action_state() const final { return current_state; }

	action_state add_observer(action_observer *o) final
	{
		observers.push_back(o);
		return current_state;
	}
	void remove_observer(action_observer *o) final { observers.remove(o); }

protected:
	template<std::ranges::range Range>
	action_impl(Range dependencies)
	:
		event_impl(UNSIGNALED),
		waiting(dependencies.begin(), dependencies.end())
	{
	}

	action_impl()
	:
		event_impl(UNSIGNALED)
	{
	}

	virtual void do_start() = 0;

	void add_dependency(event *) final;

	void final_construct() override;

	void set_action_state(action_state new_state);

private:
	action_state current_state = CREATED;

	std::vector<ptr<event>> waiting;
	std::list<ptr<action_observer>> observers;

	void register_with_dependencies();

	void on_state_change(
			event *e,
			event::event_state old_state,
			event::event_state new_state);
};

class action_observer :
	public virtual unknown
{
public:
	virtual void on_state_change(
			action *a,
			action::action_state old_state,
			action::action_state new_state) = 0;
};
