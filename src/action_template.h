#pragma once

#include <unknown.h>

class action;

class action_template :
	public virtual unknown
{
public:
	virtual void create_action(action **) const = 0;
};
