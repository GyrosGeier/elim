#pragma once

#include "action.h"

#include "mainloop.h"

#include <string>

#include <unistd.h>

class output;

class command :
	public virtual action
{
public:
	virtual void set_stdout(output &o) = 0;
};

class command_impl :
	public command,
	private action_impl,
	private mainloop::process_watcher
{
public:
	template<typename ...Deps>
	command_impl(mainloop &m, std::string const &shell_command, Deps &&...deps)
	:
		action_impl(std::forward<Deps>(deps)...),
		m(m),
		m_shell_command(shell_command)
	{
	}

	void set_stdout(output &o);

	std::string const &name() const override { return m_shell_command; }

private:
	mainloop &m;
	output *o = nullptr;
	std::string const m_shell_command;

	void do_start() override;

	void on_process_exit(int status) override;
};
