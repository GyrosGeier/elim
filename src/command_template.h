#pragma once

#include "action_template.h"

#include <string>

class environment;

class command_template :
	public action_template
{
public:
	command_template(
			environment const &env,
			std::string const &cmd)
	:
		env(env),
		cmd(cmd)
	{
	}

	environment const &env;
	std::string const cmd;

	// action_template
	void create_action(action **) const final;
};
