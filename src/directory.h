#pragma once

#include <unknown.h>

#include <filesystem>

class directory :
    public virtual unknown
{

};

class directory_impl :
    public directory
{
public:
    static constexpr struct temp_directory_tag { } temp_directory = { };

    directory_impl(temp_directory_tag const &);

protected:
    ~directory_impl() noexcept;

private:
    std::filesystem::path const path;
    bool const delete_on_destroy;

    static std::filesystem::path create_temp_directory();
};
