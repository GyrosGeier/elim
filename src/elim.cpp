#include "command.h"
#include "output.h"
#include "mainloop.h"
#include "action.h"

#include "progress.h"

#include "project_file.h"

#include "environment.h"

#include "ptr.h"

#include <iostream>
#include <exception>

#include <memory>

int main(int argc, char **argv)
try
{
	mainloop ml;

	auto p = make_object<progress, progress_impl>();

	environment local_env(ml);

	std::map<std::string, ptr<project>> projects;

	{
		enum
		{
			INITIAL,
			VALIDATE
		} state = INITIAL;

		for(char **i = argv + 1; i != argv + argc; ++i)
		{
			std::string const arg(*i);
			switch(state)
			{
			case INITIAL:
				if(arg == "--validate")
					state = VALIDATE;
				else
					throw "Invalid arg";
				break;

			case VALIDATE:
				{
					project_file p(local_env, arg);
					projects = p.projects;
				}
				state = INITIAL;
				break;
			}
		}

		switch(state)
		{
		case INITIAL:
			break;

		case VALIDATE:
			throw "File name required for --validate";
		}
	}

	output versions;

	std::list<ptr<action>> todo;

	for(auto const &i : projects)
	{
		ptr<action> previous;
		for(auto const &ii : i.second->get_build_commands())
		{
			ptr<action> a;
			ii->create_action(&a);
			if(a)
			{
				if(previous)
					a->add_dependency(previous);
				todo.push_back(a);
				previous = std::move(a);
			}
		}
	}

	{
		auto get_versions = make_object<command, command_impl>(ml, "git rev-list --reverse --all");
		get_versions->set_stdout(versions);

		todo.emplace_back(std::move(get_versions));
	}

	for(auto &i : todo)
		p->observe(i.get());

	for(auto &i : todo)
		i->start();

	ml.run();

	for(auto const &i : versions)
		std::cerr << "  " << i << std::endl;

	return 0;
}
catch(char const *msg)
{
	std::cerr << "E: " << msg << std::endl;
	return 1;
}
catch(std::exception &e)
{
	std::cerr << "E: " << e.what() << std::endl;
	return 1;
}
