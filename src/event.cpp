#include "event.h"

#include <algorithm>

event::event_state event_impl::get_event_state() const
{
	return state;
}

event::event_state event_impl::add_observer(
		event_observer *o)
{
	observers.push_back(o);
	return state;
}

void event_impl::remove_observer(
		event_observer *o)
{
	/// @todo cheaper
	/// @todo make safe reentrant with set_state
	observers.erase(std::remove(observers.begin(), observers.end(), o));
}

void event_impl::set_state(
		event::event_state const new_state)
{
	event_state const old_state = state;

	if(old_state == new_state)
		return;

	state = new_state;

	for(auto const &o : observers)
		o->on_state_change(this, old_state, new_state);
}
