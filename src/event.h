#pragma once

#include "unknown.h"
#include "ptr.h"

#include <vector>

class event_observer;

class event :
	public virtual unknown
{
public:
	enum event_state
	{
		UNSIGNALED,
		SIGNALED
	};

	virtual event_state get_event_state() const = 0;

	virtual event_state add_observer(event_observer *) = 0;
	virtual void remove_observer(event_observer *) = 0;
};

class event_observer :
	public virtual unknown
{
public:
	virtual void on_state_change(
			event *e,
			event::event_state old_state,
			event::event_state new_state) = 0;
};

class event_impl :
	public virtual event
{
public:
	// event
	event_state get_event_state() const final;
	event_state add_observer(event_observer *) final;
	void remove_observer(event_observer *) final;

protected:
	event_impl(event_state initial) : state(initial) { }

	void set_state(event_state);

private:
	event_state state;

	std::vector<ptr<event_observer>> observers;
};
