#pragma once

#include "interp.h"

#include <stdexcept>
#include <typeinfo>

class interpreter::undefined_function :
	public std::logic_error
{
public:
	undefined_function(std::string const &name)
	:
		std::logic_error("Undefined function " + name),
		name(name)
	{ }

	std::string const name;
};

class interpreter::undefined_variable :
	public std::logic_error
{
public:
	undefined_variable(std::string const &name)
	:
		std::logic_error("Undefined variable " + name),
		name(name)
	{ }

	std::string const name;
};

class interpreter::missing_argument :
	public std::logic_error
{
public:
	missing_argument(
			std::string const &function,
			std::string const &argument)
	:
		std::logic_error(
				"Function "
				+ function
				+ " requires argument "
				+ argument),
		function(function),
		argument(argument)
	{ }

	std::string const function;
	std::string const argument;
};

class interpreter::type_mismatch :
	public std::logic_error
{
public:
	type_mismatch(
			std::string const &function,
			std::string const &argument,
			std::type_info const &actual_type,
			std::type_info const &expected_type)
	:
		std::logic_error(
				"Function "
				+ function
				+ " argument "
				+ argument
				+ " has wrong type."),
		function(function),
		argument(argument),
		actual_type(actual_type),
		expected_type(expected_type)
	{ }

	std::string const function;
	std::string const argument;
	std::type_info const &actual_type;
	std::type_info const &expected_type;
};
