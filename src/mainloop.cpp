#include "mainloop.h"

#include "output.h"

#include <sys/wait.h>

#include <sys/types.h>
#include <sys/select.h>

#include <errno.h>

namespace {

extern "C" void nop(int);
void nop(int) { }

}

void mainloop::run()
{
	sigset_t oldset;
	sigset_t chld;

	::sigemptyset(&chld);
	::sigaddset(&chld, SIGCHLD);

	::sigprocmask(SIG_BLOCK, &chld, &oldset);

	::signal(SIGCHLD, &nop);

	for(;;)
	{
		bool const have_child_processes = not m_processes.empty();
		bool const have_outputs = not m_outputs.empty();

		if(not (have_child_processes or have_outputs))
			break;

		if(have_child_processes)
		{
			int status;

			pid_t pid = ::waitpid(-1, &status, WNOHANG);

			if(pid == -1)
				throw errno;

			if(pid > 0)
			{
				for(auto it = m_processes.begin(); it != m_processes.end(); /* below */)
				{
					if((*it)->pid == pid)
					{
						(*it)->on_process_exit(status);
						it = m_processes.erase(it);
						break;
					}
					++it;
				}
				continue;
			}
		}

		fd_set readable;
		int maxfd = -1;

		FD_ZERO(&readable);

		for(auto &o : m_outputs)
		{
			if(o.fd > maxfd)
				maxfd = o.fd;
			FD_SET(o.fd, &readable);
		}

		int rc = ::pselect(
				maxfd + 1,
				have_outputs ? &readable : nullptr,
				nullptr,
				nullptr,
				nullptr,
				&oldset);

		if(rc == -1)
		{
			if(errno == EINTR)
				continue;
			throw errno;
		}

		for(auto it = m_outputs.begin(); it != m_outputs.end(); /* below */)
		{
			if(FD_ISSET(it->fd, &readable))
			{
				char buffer[256];
				ssize_t count = ::read(it->fd, buffer, sizeof buffer);
				if(count == -1)
					throw errno;

				if(count == 0)
				{
					::close(it->fd);
					it = m_outputs.erase(it);
					continue;
				}

				it->o.add(std::string(buffer, count));
				++it;
			}
		}
	}

	::signal(SIGCHLD, SIG_DFL);

	::sigprocmask(SIG_SETMASK, &oldset, nullptr);
}

void mainloop::watch_process(process_watcher *watcher)
{
	m_processes.push_back(watcher);
}

void mainloop::connect(int fd, output &o)
{
	m_outputs.emplace_back( output_pipe { fd, o } );
}
