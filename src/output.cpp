#include "output.h"

void output::add(std::string const &s)
{
	if(s.empty())
		return;

	auto const nl = s.find('\n');
	if(nl == s.npos)
	{
		if(last_was_nl)
			emplace_back(s);
		else
			back().append(s);
		last_was_nl = false;
	}
	else
	{
		if(last_was_nl)
			emplace_back(s.substr(0, nl));
		else
			back().append(s.substr(0, nl));
		last_was_nl = true;
		add(s.substr(nl + 1));
	}
}
