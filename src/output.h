#pragma once

#include <list>
#include <string>

class output :
	public std::list<std::string>
{
public:
	void add(std::string const &s);

private:
	bool last_was_nl = true;
};
