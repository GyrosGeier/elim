#include "progress.h"

#include <iostream>

template<typename CharT, typename TraitsT>
std::basic_ostream<CharT, TraitsT> &operator<<(
		std::basic_ostream<CharT, TraitsT> &o,
		action::action_state s)
{
	switch(s)
	{
	case action::CREATED:	return o << "CREATED";
	case action::READY:	return o << "READY";
	case action::WAITING:	return o << "WAITING";
	case action::STARTED:	return o << "STARTED";
	case action::FINISHED:	return o << "FINISHED";
	}
	return o;
}

void progress_impl::observe(action *a)
{
	auto current_state = a->add_observer(this);

	std::cerr << "  add " << current_state << " " << a->name() << std::endl;
}

void progress_impl::on_state_change(
		action *a,
		action::action_state old_state,
		action::action_state new_state)
{
	std::cerr << "  chg " << old_state << " -> " << new_state << " " << a->name() << std::endl;
}
