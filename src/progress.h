#pragma once

#include "action.h"

class progress :
	public virtual unknown
{
public:
	virtual void observe(action *a) = 0;
};

class progress_impl :
	public progress,
	private action_observer
{
public:
	void observe(action *a);

private:
	void on_state_change(
			action *,
			action::action_state old_state,
			action::action_state new_state) override;
};
