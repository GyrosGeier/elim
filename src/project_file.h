#pragma once

#include "project.h"

#include <ptr.h>

#include <string>
#include <map>

class environment;

class project_file
{
public:
	project_file(
			environment const &env,
			std::string const &name);

	std::map<std::string, ptr<project>> projects;
};
