#include "command.h"

#include "mainloop.h"

#include "ptr.h"

#include <sys/time.h>

static timeval operator-(timeval const &lhs, timeval const &rhs)
{
	if (lhs.tv_usec < rhs.tv_usec)
		return timeval
		{
			lhs.tv_sec - rhs.tv_sec - 1,
			1000000 - rhs.tv_usec + lhs.tv_usec
		};
	else
		return timeval
		{
			lhs.tv_sec - rhs.tv_sec,
			lhs.tv_usec - rhs.tv_usec
		};
}

int main(int, char **)
{
	mainloop ml;

	auto test1 = make_object<command, command_impl>(ml, "sleep 1");
	auto test2 = make_object<command, command_impl>(ml, "sleep 2");

	timeval start;
	::gettimeofday(&start, nullptr);

	test1->start();
	test2->start();

	ml.run();

	timeval stop;
	::gettimeofday(&stop, nullptr);

	timeval diff = stop - start;

	if(diff.tv_sec != 2)
		return 1;

	return 0;
}
