#pragma once

#include <string>
#include <ptr.h>

struct boolean_value;
struct string_value;
struct array_value;
struct object_value;

struct value
{
	virtual ~value() noexcept = default;
	virtual value *clone() const = 0;
};

struct boolean_value : value
{
	boolean_value(bool b) : contents(b) { }

	boolean_value *clone() const final
	{
		return new boolean_value(*this);
	}

	bool contents;
};

struct string_value : value
{
	string_value(std::string const &s) : contents(s) { }

	string_value *clone() const final
	{
		return new string_value(*this);
	}

	std::string contents;
};

struct array_value : value
{
	array_value *clone() const final
	{
		auto ret = std::make_unique<array_value>();
		ret->contents.reserve(contents.size());
		for(auto const &i : contents)
			ret->contents.emplace_back(i->clone());
		return ret.release();
	}

	std::vector<std::unique_ptr<value>> contents;
};

struct object_value : value
{
	object_value(ptr<unknown> const &p) : contents(p) { }
	object_value(ptr<unknown> &&p) : contents(p) { }

	object_value *clone() const final
	{
		return new object_value(*this);
	}

	ptr<unknown> contents;
};
