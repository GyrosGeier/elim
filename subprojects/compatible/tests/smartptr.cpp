#include <ptr.h>

#include <catch2/catch_test_macros.hpp>

struct info
{
	enum
	{
		initial,
		active,
		destroyed
	} state = initial;
	uint32_t refcount = 0;
};

class base : public unknown
{
};

class base_impl final : public base
{
public:
	base_impl(info &i) : i(i) { i.state = i.active; i.refcount = 0; }
	~base_impl() noexcept { i.state = i.destroyed; }

	// unknown
	uint32_t add_ref() final { return ++i.refcount; }
	uint32_t release() final
	{
		--i.refcount;
		if(i.refcount > 0)
			return i.refcount;
		delete this;
		return 0;
	}

private:
	info &i;
};

class derived : public base
{
};

class derived_impl final : public derived
{
public:
	derived_impl(info &i) : i(i) { i.state = i.active; i.refcount = 0; }
	~derived_impl() noexcept { i.state = i.destroyed; }

	// unknown
	uint32_t add_ref() final { return ++i.refcount; }
	uint32_t release() final
	{
		--i.refcount;
		if(i.refcount > 0)
			return i.refcount;
		delete this;
		return 0;
	}

private:
	info &i;
};

template class ptr<base>;

TEST_CASE("Smart Pointers")
{
	SECTION("Mock Object")
	{
		info i;

		REQUIRE(i.state == i.initial);

		base *b = new base_impl(i);

		REQUIRE(i.state == i.active);
		REQUIRE(i.refcount == 0);

		REQUIRE(b->add_ref() == 1);
		REQUIRE(i.state == i.active);
		REQUIRE(i.refcount == 1);

		REQUIRE(b->add_ref() == 2);
		REQUIRE(i.state == i.active);
		REQUIRE(i.refcount == 2);

		REQUIRE(b->release() == 1);
		REQUIRE(i.state == i.active);
		REQUIRE(i.refcount == 1);

		REQUIRE(b->release() == 0);
		REQUIRE(i.state == i.destroyed);
	}

	SECTION("Constructors")
	{
		SECTION("Default Construction")
		{
			ptr<base> p;

			// REQUIRE(p == 0);
			REQUIRE(p == nullptr);
			// REQUIRE(0 == p);
			REQUIRE(nullptr == p);
		}

		SECTION("Construct from Null Pointer Constant")
		{
			ptr<base> p(nullptr);

			REQUIRE(p == nullptr);
			REQUIRE(nullptr == p);
		}

		SECTION("Construct from Base Null Pointer")
		{
			base *b = nullptr;

			ptr<base> p(b);

			REQUIRE(p == nullptr);
			REQUIRE(nullptr == p);
		}

		SECTION("Construct from Derived Null Pointer")
		{
			base *b = nullptr;

			ptr<base> p(b);

			REQUIRE(p == nullptr);
			REQUIRE(nullptr == p);
		}

		SECTION("Construction with Base Pointer")
		{
			info i;

			{
				ptr<base> p(new base_impl(i));

				REQUIRE(i.state == i.active);
				REQUIRE(i.refcount == 1);
			}

			REQUIRE(i.state == i.destroyed);
		}

		SECTION("Construction with Derived Pointer")
		{
			info i;

			{
				ptr<base> p(new derived_impl(i));

				REQUIRE(i.state == i.active);
				REQUIRE(i.refcount == 1);
			}

			REQUIRE(i.state == i.destroyed);
		}

		SECTION("Copy Construct from Empty")
		{
			ptr<base> p1 = nullptr;
			ptr<base> p2(p1);

			REQUIRE(p1 == nullptr);
			REQUIRE(p2 == nullptr);
		}

		SECTION("Copy Construct from Object")
		{
			info i;

			{
				ptr<base> p1(new base_impl(i));
				ptr<base> p2(p1);

				REQUIRE(p1 != nullptr);
				REQUIRE(p2 != nullptr);

				REQUIRE(i.refcount == 2);
			}

			REQUIRE(i.state == i.destroyed);
		}

		SECTION("Construct from Empty Derived")
		{
			ptr<derived> p1 = nullptr;
			ptr<base> p2(p1);

			REQUIRE(p1 == nullptr);
			REQUIRE(p2 == nullptr);
		}

		SECTION("Construct from Derived")
		{
			info i;

			{
				ptr<derived> p1(new derived_impl(i));
				ptr<base> p2(p1);

				REQUIRE(p1 != nullptr);
				REQUIRE(p2 != nullptr);

				REQUIRE(i.refcount == 2);
			}

			REQUIRE(i.state == i.destroyed);
		}

		SECTION("Move Construct from Empty")
		{
			ptr<base> p1 = nullptr;
			ptr<base> p2(std::move(p1));

			REQUIRE(p1 == nullptr);
			REQUIRE(p2 == nullptr);
		}

		SECTION("Move Construct from Object")
		{
			info i;

			{
				ptr<base> p1(new base_impl(i));
				ptr<base> p2(std::move(p1));

				REQUIRE(p1 == nullptr);
				REQUIRE(p2 != nullptr);
				REQUIRE(i.state == i.active);
				REQUIRE(i.refcount == 1);
			}

			REQUIRE(i.state == i.destroyed);
		}

		SECTION("Move Construct from Derived Empty")
		{
			ptr<derived> p1 = nullptr;
			ptr<base> p2 = std::move(p1);

			REQUIRE(p1 == nullptr);
			REQUIRE(p2 == nullptr);
		}

		SECTION("Move Construct from Derived")
		{
			info i;

			{
				ptr<derived> p1(new derived_impl(i));
				ptr<base> p2 = std::move(p1);

				REQUIRE(p1 == nullptr);
				REQUIRE(p2 != nullptr);
				REQUIRE(i.state == i.active);
				REQUIRE(i.refcount == 1);
			}

			REQUIRE(i.state == i.destroyed);
		}
	}

	SECTION("Accessors")
	{
		info i;
		base *b = new base_impl(i);
		ptr<base> p(b);

		REQUIRE(i.state == i.active);
		REQUIRE(i.refcount == 1);

		SECTION("Cast to Pointer")
		{
			base *b2 = p;
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
			REQUIRE(b2 == b);
		}

		SECTION("Dereference with *")
		{
			base &b2 = *p;
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
			REQUIRE(&b2 == b);
		}

		SECTION("Dereference with ->")
		{
			base *b2 = p.operator->();
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
			REQUIRE(b2 == b);
		}

		SECTION("Get address with &")
		{
			ptr<base> p2;
			*&p2 = p.detach();
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
			REQUIRE(p == nullptr);
			REQUIRE(p2 != nullptr);
		}

		SECTION("get()")
		{
			base *b2 = p.get();
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
			REQUIRE(b2 == b);
		}

		SECTION("reset()")
		{
			p.reset();
			REQUIRE(i.state == i.destroyed);
		}
	}

	SECTION("Assignment to Empty")
	{
		info i;
		ptr<base> p;

		SECTION("Assign Empty from Null Pointer Constant")
		{
			p = nullptr;

			REQUIRE(p == nullptr);
		}

		SECTION("Assign Empty from Pointer")
		{
			p = new base_impl(i);

			REQUIRE(p != nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
		}

		SECTION("Assign Empty from Derived Pointer")
		{
			p = new derived_impl(i);

			REQUIRE(p != nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
		}

		SECTION("Assign Empty from Null Pointer")
		{
			p = static_cast<base *>(nullptr);

			REQUIRE(p == nullptr);
		}

		SECTION("Assign Empty from Derived Null Pointer")
		{
			p = static_cast<derived *>(nullptr);

			REQUIRE(p == nullptr);
		}

		SECTION("Self-Assign while Empty")
		{
			p = p;

			REQUIRE(p == nullptr);
		}

		SECTION("Assign Empty from Object")
		{
			ptr<base> b(new base_impl(i));

			p = b;

			REQUIRE(p != nullptr);
			REQUIRE(b != nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 2);
		}

		SECTION("Assign Empty from Derived Object")
		{
			ptr<derived> d(new derived_impl(i));

			p = d;

			REQUIRE(p != nullptr);
			REQUIRE(d != nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 2);
		}

		SECTION("Assign Empty from Empty")
		{
			ptr<base> b;

			p = b;

			REQUIRE(p == nullptr);
			REQUIRE(b == nullptr);
		}

		SECTION("Assign Empty from Derived Empty")
		{
			ptr<derived> d;

			p = d;

			REQUIRE(p == nullptr);
			REQUIRE(d == nullptr);
		}

		SECTION("Move Assign Empty from Object")
		{
			ptr<base> b(new base_impl(i));

			p = std::move(b);

			REQUIRE(p != nullptr);
			REQUIRE(b == nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
		}

		SECTION("Move Assign Empty from Derived Object")
		{
			ptr<derived> d(new derived_impl(i));

			p = std::move(d);

			REQUIRE(p != nullptr);
			REQUIRE(d == nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
		}

		SECTION("Move Assign Empty from Empty")
		{
			ptr<base> b;

			p = std::move(b);

			REQUIRE(p == nullptr);
			REQUIRE(b == nullptr);
		}

		SECTION("Move Assign Empty from Derived Empty")
		{
			ptr<derived> d;

			p = std::move(d);

			REQUIRE(p == nullptr);
			REQUIRE(d == nullptr);
		}
	}

	SECTION("Assignment")
	{
		info i, ii;
		ptr<base> p(new base_impl(ii));

		SECTION("Assign from Null Pointer Constant")
		{
			p = nullptr;

			REQUIRE(p == nullptr);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Assign from Pointer")
		{
			p = new base_impl(i);

			REQUIRE(p != nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Assign from Same Pointer")
		{
			p = p.get();

			REQUIRE(p != nullptr);
			REQUIRE(ii.state == ii.active);
			REQUIRE(ii.refcount == 1);
		}

		SECTION("Assign from Derived Pointer")
		{
			p = new derived_impl(i);

			REQUIRE(p != nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Assign from Null Pointer")
		{
			p = static_cast<base *>(nullptr);

			REQUIRE(p == nullptr);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Assign from Derived Null Pointer")
		{
			p = static_cast<derived *>(nullptr);

			REQUIRE(p == nullptr);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Self-Assign")
		{
			p = p;

			REQUIRE(p != nullptr);
			REQUIRE(ii.state == ii.active);
			REQUIRE(ii.refcount == 1);
		}

		SECTION("Assign from Object")
		{
			ptr<base> b(new base_impl(i));

			p = b;

			REQUIRE(p != nullptr);
			REQUIRE(b != nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 2);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Assign from Same Object")
		{
			ptr<base> b(p);

			p = b;

			REQUIRE(p != nullptr);
			REQUIRE(b != nullptr);
			REQUIRE(ii.state == ii.active);
			REQUIRE(ii.refcount == 2);
		}

		SECTION("Assign from Derived Object")
		{
			ptr<derived> d(new derived_impl(i));

			p = d;

			REQUIRE(p != nullptr);
			REQUIRE(d != nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 2);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Assign from Empty")
		{
			ptr<base> b;

			p = b;

			REQUIRE(p == nullptr);
			REQUIRE(b == nullptr);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Assign from Derived Empty")
		{
			ptr<derived> d;

			p = d;

			REQUIRE(p == nullptr);
			REQUIRE(d == nullptr);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Move Assign from Object")
		{
			ptr<base> b(new base_impl(i));

			p = std::move(b);

			REQUIRE(p != nullptr);
			REQUIRE(b == nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Move Assign from Same Object")
		{
			ptr<base> b(p);

			p = std::move(b);

			REQUIRE(p != nullptr);
			REQUIRE(b == nullptr);
			REQUIRE(ii.state == ii.active);
			REQUIRE(ii.refcount == 1);
		}

		SECTION("Move Assign from Derived Object")
		{
			ptr<derived> d(new derived_impl(i));

			p = std::move(d);

			REQUIRE(p != nullptr);
			REQUIRE(d == nullptr);
			REQUIRE(i.state == i.active);
			REQUIRE(i.refcount == 1);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Move Assign from Empty")
		{
			ptr<base> b;

			p = std::move(b);

			REQUIRE(p == nullptr);
			REQUIRE(b == nullptr);
			REQUIRE(ii.state == ii.destroyed);
		}

		SECTION("Move Assign from Derived Empty")
		{
			ptr<derived> d;

			p = std::move(d);

			REQUIRE(p == nullptr);
			REQUIRE(d == nullptr);
			REQUIRE(ii.state == ii.destroyed);
		}
	}
}
